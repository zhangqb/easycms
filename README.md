#easycms  持续开发中。。。别忘了给star一下
#招募不到ui
没有ui自己干 加群 451615244
###开发环境
maven， eclipse neon，jdk8(编译1.7)，tomcat，mysql5.6
###简介
Java EasyCms 使用最简单性能最高的框架，将cms系统简单到极致，灵活的栏目扩展，快速的构建普通网站
生成的静态演示站（测试）： www.j4cms.com
###静态网站生成工具
teleport ultra 请求本地启动的项目即可，拷贝出来就能显示，报错就覆盖一下assets文件夹
###纯html
生成后就可以部署到最便宜的php空间上了，备案、域名、空间推荐阿里云 https://promotion.aliyun.com/ntms/act/ambassador/sharetouser.html?userCode=jolyjv1d&productCode=qingcloud&utm_source=jolyjv1d
###技术框架工具
java，servlet，guice4， mybatis3， jstl，bootstrap2，bootstrap3，jerichotab，jqGrid，jquery，jquery-validation，layer，My97DatePicker，ueditor，zTree等等
###登录
localhost:8080/easycms/cms  账户root 密码easycms在shiro.ini 文件中配置
前端 localhost:8080/easycms/index,演示模板为商城
###注意
使用ueditor时要使用根目录，否则上传的图片不显示，即打包成 ROOT.war放到tomcat 的webapps下面
###几张截图
![输入图片说明](https://git.oschina.net/uploads/images/2017/0812/113713_60f5d724_370580.png "QQ图片20170812111809.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0812/113721_66620f65_370580.png "QQ图片20170812113229.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0812/113730_fc5ca86f_370580.png "QQ图片20170729165140.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0812/113738_6364e2b1_370580.png "QQ图片20170812073235.png")
![输入图片说明](https://git.oschina.net/uploads/images/2017/0812/113745_752d6023_370580.png "QQ图片20170812073303.png")